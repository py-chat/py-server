import socket, pickle
import _thread as thread


class Server():
    def __init__(self, host, port):
        try:
            self.host = host
            self.port = port
            self.s = socket.socket()
            self.connections = []
        except socket.error as msg:
            print(str(msg))

    def main(self):
        self.bind_socket()
        while True:
            con = self.accept_connection()
            thread.start_new_thread(self.on_client_connection,(con,))
        self.s.close()

    def bind_socket(self):
        try:
            self.s.bind((self.host, self.port))
            self.s.listen(5)
        except socket.error as msg:
            print(str(msg))
            self.bind_socket()

    def accept_connection(self):
        conn, address = self.s.accept()
        print("Connection established")
        print("IP: {0} | PORT: {1} \n".format(address[0], address[1]))
        return conn

    def on_client_connection(self, connection):
        self.connections.append(connection)
        connection.send(str.encode("Connection established\n"))
        while True:
            name, text = self.recieve_data(connection)
            msg = "{0}: {1}".format(name, text)
            print(msg)
            self.send(msg, connection)

    def recieve_data(self, conn):
        data = conn.recv(1024)
        data_arr = pickle.loads(data)
        return (tuple(data_arr))

    def send(self, text, from_adr):
        data = str.encode(text)
        for con in self.connections:
            if from_adr is not con:
                con.send(data)


if __name__ == "__main__":
    port = input("port: ")
    server = Server("127.0.0.1", int(port))
    server.main()
